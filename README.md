# gnome-shell-extension-lockkeys

It shows notifications about numlock or capslock state change.

https://github.com/kazysmaster/gnome-shell-extension-lockkeys

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-lockkeys.git
```

**NOTE:** Extensions shows this extension as **Lock Keys**

